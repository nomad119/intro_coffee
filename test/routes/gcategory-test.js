var chai = require('chai');
var chaiHttp = require('chai-http');
var gserver = require('../../server');
var should = chai.should();

chai.use(chaiHttp);
var catID = 0;
describe('Category Calls', () =>{
    afterEach( () =>{
        gserver.close();   
    });
    it('gets all Categories', function(done){
        chai.request(gserver)
        .get('/api/cat/')
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('creates a category', function(done){
        chai.request(gserver)
        .post('/api/cat/add')
        .send({"name":"Physics"})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.name.should.have.equal('Physics');
            catID = res.body.insertId;
            done();
        });
    });
    it('gets one category',  function(done){
        chai.request(gserver)
        .get('/api/cat/'+catID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('updates a category', function(done){
        chai.request(gserver)
        .put('/api/cat/update/'+catID)
        .send({"name":"Physics"})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.name.should.have.equal('Physics');
            done();
        });
    });
    it('deletes a category', function(done){
        chai.request(gserver)
        .delete('/api/cat/del/'+catID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('isDeleted');
            res.body.isDeleted.should.have.equal('true');
            done();
        });
    });


});