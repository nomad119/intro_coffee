var chai = require('chai');
var chaiHttp = require('chai-http');
var gserver = require('../../server');
var should = chai.should();

chai.use(chaiHttp);
var modID = 0;
describe('Module Calls', () =>{
    afterEach( () =>{
        gserver.close();   
    });
    it('gets all Modules', function(done){
        chai.request(gserver)
        .get('/api/mod/')
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('creates a Module', function(done){
        chai.request(gserver)
        .post('/api/mod/add')
        .send({"score":80.0, "mName" : "Zombie Game", "catID":1,"sConstraint":100,"studID":3})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('mName');
            res.body.mName.should.have.equal('Zombie Game');
            modID = res.body.insertId;
            done();
        });
    });
    it('gets one module',  function(done){
        chai.request(gserver)
        .get('/api/mod/'+modID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('updates a module', function(done){
        chai.request(gserver)
        .put('/api/mod/update/'+modID)
        .send({"score":80.0, "mName" : "Zombie Game", "catID":1,"sConstraint":100,"studID":3})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('mName');
            res.body.mName.should.have.equal('Zombie Game');
            done();
        });
    });
    it('deletes a module', function(done){
        chai.request(gserver)
        .delete('/api/mod/del/'+modID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('isDeleted');
            res.body.isDeleted.should.have.equal('true');
            done();
        });
    });


});