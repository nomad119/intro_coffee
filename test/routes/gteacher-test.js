var chai = require('chai');
var chaiHttp = require('chai-http');
var gserver = require('../../server');
var should = chai.should();

chai.use(chaiHttp);
var teachID = 0;
describe('Teachers Calls', () =>{
    afterEach( () =>{
        gserver.close();
    });
    it('gets all teachers', function(done){
        chai.request(gserver)
        .get('/api/teacher/')
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('creates a teacher', function(done){
        chai.request(gserver)
        .post('/api/teacher/add')
        .send({"name":"Justin","admin":"1"})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.name.should.have.equal('Justin');
            teachID = res.body.insertId;
            done();
        });
    });
    it('gets one teacher',  function(done){
        chai.request(gserver)
        .get('/api/teacher/'+teachID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('updates a teacher', function(done){
        chai.request(gserver)
        .put('/api/teacher/update/'+teachID)
        .send({"name":"James","admin":"1"})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.name.should.have.equal('James');
            done();
        });
    });
    it('deletes a teacher', function(done){
        chai.request(gserver)
        .delete('/api/teacher/del/'+teachID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('isDeleted');
            res.body.isDeleted.should.have.equal('true');
            done();
        });
    });


});