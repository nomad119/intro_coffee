var chai = require('chai');
var chaiHttp = require('chai-http');
var gserver = require('../../server');
var should = chai.should();

chai.use(chaiHttp);
var scoreID = 0;
describe('Score Calls', () =>{
    afterEach( () =>{
        gserver.close();   
    });
    it('gets all Scores', function(done){
        chai.request(gserver)
        .get('/api/score/')
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('creates a score', function(done){
        chai.request(gserver)
        .post('/api/score/add')
        .send({"stud":2, "teach" : 2, "sCat": 1})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('stud');
            res.body.stud.should.have.equal(2);
            scoreID = res.body.insertId;
            done();
        });
    });
    it('gets one score',  function(done){
        chai.request(gserver)
        .get('/api/score/'+scoreID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('updates a score', function(done){
        chai.request(gserver)
        .put('/api/score/update/'+scoreID)
        .send({"stud":2, "teach" : 2, "sCat": 1})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('stud');
            res.body.stud.should.have.equal(2);
            done();
        });
    });
    it('deletes a score', function(done){
        chai.request(gserver)
        .delete('/api/score/del/'+scoreID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('isDeleted');
            res.body.isDeleted.should.have.equal('true');
            done();
        });
    });


});