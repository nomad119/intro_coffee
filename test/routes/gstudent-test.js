var chai = require('chai');
var chaiHttp = require('chai-http');
var gserver = require('../../server');
var should = chai.should();

chai.use(chaiHttp);
var studID = 0;
describe('Student Calls', () =>{
    afterEach( () =>{
        gserver.close();
    });
    it('gets all students', function(done){
        chai.request(gserver)
        .get('/api/student/')
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('creates a student', function(done){
        chai.request(gserver)
        .post('/api/student/add')
        .send({"name":"Justin","teach":"2"})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.name.should.have.equal('Justin');
            studID = res.body.insertId;
            done();
        });
    });
    it('gets one student',  function(done){
        chai.request(gserver)
        .get('/api/student/'+studID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('array');
            done();
        });
    });
    it('updates a student', function(done){
        chai.request(gserver)
        .put('/api/student/update/'+studID)
        .send({"name":"Justin","teach":"2"})
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('name');
            res.body.name.should.have.equal('Justin');
            done();
        });
    });
    it('deletes a student', function(done){
        chai.request(gserver)
        .delete('/api/student/del/'+studID)
        .end(function(err, res){
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('isDeleted');
            res.body.isDeleted.should.have.equal('true');
            done();
        });
    });


});