//define dependencies for the application
var express = require('express');
var mysql   = require('mysql');
var router= express.Router();
var app = express();
var path    = require("path");
var bodyParser = require('body-parser');
//define routes for api
var beans = require("./Routes/beanRoutes");
var recipes = require("./Routes/recipeRoutes")
var coffeeUsers= require("./Routes/userRoutes");
var brewTypes = require("./Routes/brewTypeRoutes");

//create and define mysql pool
var pool = mysql.createConnection({
   host     :'localhost',
   user     :'root',
   password :'admin',
   database :'coffee'
});
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//express call to use routes
app.use(beans);
app.use(recipes);
app.use(coffeeUsers);
app.use(brewTypes);

//test query to see if we have a connection to the database
pool.query('SELECT * FROM `couser` WHERE `Fname`="Victor"', function (error, results, fields) {
    if (error) throw error;
    console.log('The name is: ', results[0].Fname);
  });
//generic get route to test express
app.get('/',function(req,res){
    res.render((path.join(__dirname + '/views/recipe.html')));
});
app.get('/beans', function(req,res){

        res.render((path.join(__dirname + '/views/bean.html')));
});
console.log("It's happening");
app.listen(8080);