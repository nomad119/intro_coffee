//define dependencies for the application
var express = require('express');
var mysql = require('mysql');
var router = express.Router();
var app = express();
var path = require("path");
var bodyParser = require('body-parser');
//define routes for api
var gadmin = require("./Routes/gadmin");
var gCat = require("./Routes/gcategory");
var gMod = require("./Routes/gmodule");
var gScore = require("./Routes/gscore");
var gStudent = require("./Routes/gstudent");
var gTeacher = require("./Routes/gteacher");
var port = 8081;
var cors = require('cors');


app.use(cors());
//create and define mysql pool
var pool = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public/views');
app.use('/styles', express.static(__dirname + "/styles"));
app.use('/controllers', express.static(__dirname + "/controllers"));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
//express call to use routes
app.use(gadmin);
app.use(gCat);
app.use(gMod);
app.use(gScore);
app.use(gStudent);
app.use(gTeacher);

//test query to see if we have a connection to the database

//generic get route to test express
app.get('/', function (req, res) {
    res.render((path.join(__dirname + '/views/adminIndex.html')));
});
app.get('/beans', function (req, res) {

    res.render((path.join(__dirname + '/views/bean.html')));
});
console.log("EdTech server up at: "+ port);
module.exports =app.listen(port);