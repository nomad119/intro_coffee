var express = require('express');
var app = module.exports= express();
var router= express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);
var mysql   = require('mysql');
var pool = mysql.createPool({
    host     :'localhost',
    user     :'root',
    password :'admin',
    database :'coffee'
});

//mysql querys for brewtypes
var insertBrewType = "INSERT INTO `brewtype`(BName)Values(?)";
var getbrewTypeById = "SELECT * FROM `brewtype` WHERE BTypeID =?";
var getAllBrewTypes = "SELECT * FROM `brewtype`";
var deleteBrewType = "DELETE FROM `brewtype` WHERE BTypeID = ?";
var updateBrewType = "UPDATE `brewtype` SET BName = ?  WHERE BTypeID = ?"

//define brew routes

router.route('/brewtypes/add')
//add brewtypes to server
    .post(function(req,res){
        console.log(req.body);
        var BName = pool.escape(req.body.Bname);
        pool.query(
            insertBrewType,
            [BName],
            function(error,results,fields){
            if(error) throw error;
            res.json("BrewTypeAdded");
            console.log('Brew Type added');
        })
});

router.route('/brewtypes/')
//get all brewtypes
    .get(function(req,res){
        pool.query(getAllBrewTypes,function(error,results,fields){
            if(error) throw error;
            console.log(results);
            res.json(results);
            console.log('Getting All brew types');
        }) 
});

router.route('/brewtypes/:id')
//get brewtype by btypeid
    .get(function(req,res){
        var BTypeID=req.params.id
        pool.query(getbrewTypeById,BTypeID,function(error,results,fields){
            if(error) throw error;
            console.log('Recipes Found');
            res.json(results);
        })
});
router.route('/brewtypes/update/:id')
//update user by id    
    .post(function(req,res){
        console.log(req.body);
        var userBTypeID = req.body.id;
        var Bname = pool.escape(req.body.Bname);
        pool.query(
            updateBrewType,
            [Bname,userBTypeID],
            function(error,results,fields){
            if(error) throw error;
            res.json("recipe_Added");
            console.log('New recipe added');
        })
});
router.route('/brewtypes/del/:id')
//delete recipe by id
    .post(function(req,res){
        var BID=req.params.id;
        pool.query(deleteBrewType,BID,function(error,results,fields){
            if(error) throw error;
            console.log('Recipe Deleted');
        })    
});