var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql querys for the scores
var insertScore = "INSERT INTO `gscore`(teach,stud,sCat)Values(?,?,?)";
var getScoreById = "SELECT * FROM `gscore` WHERE scoreID =?";
var getAllScores = "SELECT * FROM `gscore`";
var deleteScore = "DELETE FROM `gscore` WHERE scoreID = ?";
var updateScore = "UPDATE `gscore`  SET teach = ?, stud= ?, sCat=? WHERE scoreID=?";
//define score routes

router.route('/score/add')
    //add score to server
    .post(function (req, res) {
        var scoreTeach = req.body.teach;
        var scoreStud = req.body.stud;
        var scoreSCat = req.body.sCat;
        pool.query(
            insertScore, [scoreTeach, scoreStud, scoreSCat],
            function (error, results, fields) {
                if (error) throw error;
                res.json({'teach':req.body.teach, 'stud':req.body.stud,'sCat':req.body.sCat,'insertID':results.insertId});
            });
    });
router.route('/score/')
    //get all scores
    .get(function (req, res) {
        pool.query(getAllScores, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });
    });

router.route('/score/:id')
    //get score by id
    .get(function (req, res) {
        var userscoreID = req.params.id;
        pool.query(getScoreById, userscoreID, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });

    });
router.route('/score/update/:id')
    //update Score by id    
    .put(function (req, res) {
        var scoreTeach = req.body.teach;
        var scoreStud = req.body.stud;
        var scoreSCat = req.body.sCat;
        var userscoreID = req.params.id;
        pool.query(
            updateScore, [scoreTeach, scoreStud, scoreSCat, userscoreID],
            function (error, results, fields) {
                if (error) throw error;
                res.json(req.body);
            });
    });

router.route('/score/del/:id')
    //delete Score
    .delete(function (req, res) {
        var userscoreID = req.params.id;
        pool.query(deleteScore, userscoreID, function (error, results, fields) {
            if (error) throw error;
            res.json({id: req.params.id, isDeleted:'true'});
        });
    });