var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//---------------------------------------------
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql querys for the scores
var insertStudent = "INSERT INTO `gstudent`(name,teach)Values(?,?)";
var getStudentById = "SELECT * FROM `gstudent` WHERE studID =?";
//var getAllStuds = "SELECT * FROM `gstudent`";
var deleteStudent = "DELETE FROM `gstudent` WHERE studID = ?";
var updateStudent = "UPDATE `gstudent`  SET name = ?, teach= ? WHERE studID=?";
var getAllStuds = "SELECT gstudent.studID, gStudent.name AS studentName, gmodule.mName AS moduleName,gmodule.score AS studentScore, gmodule.sConstraint as maxScore, gcategory.name AS categoryName,gstudent.studID FROM `gstudent`, `gmodule`, `gcategory`, `gscore`WHERE gstudent.studID = gmodule.studID AND gstudent.studID = gscore.stud AND gscore.sCat = gcategory.catID"; 
//define score routes

router.route('/student/add')
    //add students to server
    .post(function (req, res) {
        var studName = req.body.name;
        var studTeach = req.body.teach;
        pool.query(
            insertStudent, [studName, studTeach],
            function (error, results, fields) {
                if (error) throw error;
                res.json({'teach':req.body.teach, 'name':req.body.name,'insertID':results.insertId});
            
            });
    });
router.route('/student/')
    //get all student
    .get(function (req, res) {
        pool.query(getAllStuds, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });
    });

router.route('/student/:id')
    //get students by id
    .get(function (req, res) {
        var userstudID = req.params.id;
        pool.query(getStudentById, userstudID, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });

    });
router.route('/student/update/:id')
    //update student by id
    .put(function (req, res) {
        var studName = req.body.name;
        var studTeach = req.body.teach;
        var userstudID = req.params.id;
        pool.query(
            updateStudent, [studName, studTeach, userstudID],
            function (error, results, fields) {
                if (error) throw error;
                res.json(req.body);
            });
    });

router.route('/student/del/:id')
    //delete Student
    .delete(function (req, res) {
        var userstudID = req.params.id;
        pool.query(deleteStudent, userstudID, function (error, results, fields) {
            if (error) throw error;
            res.json({id: req.params.id, isDeleted:'true'});
        });
    });
