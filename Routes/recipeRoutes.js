var express = require('express');
var app = module.exports= express();
var router= express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);
var mysql   = require('mysql');
var pool = mysql.createPool({
    host     :'localhost',
    user     :'root',
    password :'admin',
    database :'coffee'
});

//mysql commands for the users
var insertRecipe = "INSERT INTO `recipe`(userID,coffeeID,BTypeID,brewTime,CMass,WMass,WTemp)Values(?,?,?,?,?,?,?)";
var getRecipeById = "SELECT * FROM `recipe` WHERE recipeID =?";
var getAllRecipes = "SELECT * FROM `recipe`as r, `brewtype`as b, `cobean` as c WHERE r.BTypeID = b.BTypeID and r.coffeeID = c.coffeeID";
var getUserRecipes = "Select * FROM `recipe` as r, `brewtype`as b, `cobean` as c  WHERE r.userID =? and r.BTypeID = b.BTypeID and r.coffeeID = c.coffeeID "
var deleteRecipe = "DELETE FROM `recipe` WHERE recipeID = ?";
var updateRecipe = "UPDATE `recipe`  SET userID = ?, coffeeID= ?, BTypeID= ?, brewTime=?,CMass =?,WMass=?,WTemp=? WHERE recipeID = ?"
var getbrewByID = "SELECT * FROM `brewType` WHERE BTypeID=?";

//define routes for users
router.route('/recipes/add')
//add recipe to server
    .post(function(req,res){
        console.log(req.body);
        var userID = JSON.parse(req.body.userID);
        console.log(userID);
        var coffeeID = JSON.parse(req.body.coffeeID);
        console.log(coffeeID);
        var BTypeID = JSON.parse(req.body.BTypeID);
        console.log(BTypeID);
        var brewTime = JSON.parse(req.body.brewTime);
        var CMass = JSON.parse(req.body.CMass);
        var WMass = JSON.parse(req.body.WMass);
        var WTemp = JSON.parse(req.body.WTemp);
        pool.query(
            insertRecipe,
            [userID,coffeeID,BTypeID,brewTime,CMass,WMass,WTemp],
            function(error,results,fields){
            if(error) throw error;
            res.json("Recipe_added");
            console.log('New Recipe added');
        })
});
router.route('/recipes/')
.get(function(req,res){
    pool.query(getAllRecipes,function(error,results,fields){
        if(error) throw error;
        res.json(results);
        console.log('Getting All Recipes');
    }) 
});
router.route('/recipes/couser/:id')
//get recipe by id
    .get(function(req,res){
    var userRepID=req.params.id
    pool.query(getUserRecipes,userRepID,function(error,results,fields){
        if(error) throw error;
        console.log('Recipe Found');
        res.json(results);
    })
    });
router.route('/recipes/:id')
//get recipe by id
    .get(function(req,res){
    var userRepID=req.params.id
    pool.query(getRecipeById,userRepID,function(error,results,fields){
        if(error) throw error;
        console.log('Recipe Found');
        res.json(results);
    })
    });
    router.route('/recipes/update/:id')
//update recipe by id    
    .post(function(req,res){
        console.log(req.body);
        var recipeID = JSON.parse(req.body.id);
        var userID = JSON.parse(req.body.userID);
        var coffeeID = JSON.parse(req.body.coffeeID);
        var BTypeID = JSON.parse(req.body.BTypeID);
        var brewTime = JSON.parse(req.body.brewTime);
        var CMass = JSON.parse(req.body.CMass);
        var WMass = JSON.parse(req.body.WMass);
        var WTemp = JSON.parse(req.body.WTemp);
        pool.query(
            updateRecipe,
            [userID,coffeeID,BTypeID,brewTime,CMass,WMass,WTemp,recipeID],
            function(error,results,fields){
            if(error) throw error;
            res.json("recipe_Added");
            console.log('New recipe added');
        })
    });
//delete recipe by id
router.route('/recipes/del/:id')
    .post(function(req,res){
        var userRepID=req.params.id;
        pool.query(deleteRecipe,userRepID,function(error,results,fields){
            if(error) throw error;
            console.log('Recipe Deleted');
        })    
});