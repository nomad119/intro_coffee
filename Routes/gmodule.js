var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql querys for the modules
var insertModule = "INSERT INTO `gmodule`(score,mName,catID,sConstraint,studID)Values(?,?,?,?,?)";
var getModsById = "SELECT * FROM `gmodule` WHERE modID =?";
var getAllMods = "SELECT * FROM `gmodule`";
var deleteModule = "DELETE FROM `gmodule` WHERE modID = ?";
var updateModule = "UPDATE `gmodule`  SET score = ?, mName= ?, catID=?, sConstraint =?, studID =? WHERE modID=?";
//define module routes

router.route('/mod/add')
    //add module to server
    .post(function (req, res) {
        var modScore = req.body.score;
        var modmName = req.body.mName;
        var modcatID = req.body.catID;
        var modSConstraint = req.body.sConstraint;
        var modStudID = req.body.studID;
        pool.query(
            insertModule, [modScore, modmName, modcatID, modSConstraint, modStudID],
            function (error, results, fields) {
                if (error) throw error;
                res.json({'mName':req.body.mName,'catID':req.body.catID,'sConstraint':req.body.sConstraint,
                'score':req.body.score,'studID':req.body.studID,'insertId':results.insertId});
            });
    });
router.route('/mod/')
    //get all cats
    .get(function (req, res) {
        pool.query(getAllMods, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });
    });

router.route('/mod/:id')
    //get category by id
    .get(function (req, res) {
        var usermodID = req.params.id;
        pool.query(getModsById, usermodID, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });

    });
router.route('/mod/update/:id')
    //update module by id    
    .put(function (req, res) {
        var modScore = req.body.score;
        var modmName = req.body.mName;
        var modcatID = req.body.catID;
        var modSConstraint = req.body.sConstraint;
        var modStudID = req.body.studID;
        var usermodID = req.params.id;
        pool.query(
            updateModule, [modScore, modmName, modcatID, modSConstraint, modStudID, usermodID],
            function (error, results, fields) {
                if (error) throw error;
                res.json(req.body);
            });
    });

router.route('/mod/del/:id')
    //delete module
    .delete(function (req, res) {
        var usermodID = req.params.id;
        pool.query(deleteModule, usermodID, function (error, results, fields) {
            if (error) throw error;
            res.json({id: req.params.id, isDeleted:'true'});
        });
    });