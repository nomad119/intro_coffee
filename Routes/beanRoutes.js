var express = require('express');
var app = module.exports= express();
var router= express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);
var mysql   = require('mysql');
var pool = mysql.createPool({
    host     :'localhost',
    user     :'root',
    password :'admin',
    database :'coffee'
});

//mysql querys for the coffee beans
var insertBean = "INSERT INTO `cobean`(CName,origin,TNotes,FNotes)Values(?,?,?,?)";
var getBeanById = "SELECT * FROM `cobean` WHERE coffeeID =?";
var getAllBeans = "SELECT * FROM `cobean`";
var deleteBean = "DELETE FROM `cobean` WHERE coffeeID = ?";
var updateBean = "UPDATE `cobean`  SET CName = ?, origin= ?, TNotes= ?, FNotes=? WHERE coffeeID = ?"
//define bean routes

router.route('/beans/add')
//add bean to server
    .post(function(req,res){
        console.log(req.body);
        var coffeeName = req.body.CName;
        var cOrigin = pool.escape(req.body.origin);
        var TNotes = pool.escape(req.body.TNotes);
        var FNotes = pool.escape(req.body.FNotes);
        pool.query(
            insertBean,
            [coffeeName,cOrigin,TNotes,FNotes],
            function(error,results,fields){
            if(error) throw error;
            res.json("Coffee_Added");
            console.log('New coffee added');
        })
});
router.route('/beans/')
//get all beans
    .get(function(req,res){
        pool.query(getAllBeans,function(error,results,fields){
            if(error) throw error;
            res.json(results);
            console.log('Getting All Coffee');
        }) 
});

router.route('/beans/:id')
//get bean by id
    .get(function(req,res){
    var userCoffeeID=req.params.id
    pool.query(getBeanById,userCoffeeID,function(error,results,fields){
        if(error) throw error;
        console.log('Coffee Found');
        res.json(results);
    })

});
router.route('/beans/update/:id')
//update bean by id    
    .post(function(req,res){
        console.log(req.body);
        var bCoffeeID=req.params.id
        var bcoffeeName = req.body.CName;
        var bcOrigin = req.body.origin;
        var bTNotes = req.body.TNotes;
        var bFNotes = req.body.FNotes;
        console.log(bCoffeeID);
        console.log(bcoffeeName);
        pool.query(
            updateBean,
            [bcoffeeName,bcOrigin,bTNotes,bFNotes,bCoffeeID],
            function(error,results,fields){
            if(error) throw error;
            res.json("bean updated");
            console.log('bean updated');
        })
});

router.route('/beans/del/:id')
//delete bean by id
    .post(function(req,res){
        var userCoffeeID=req.params.id;
        pool.query(deleteBean,userCoffeeID,function(error,results,fields){
            if(error) throw error;
            console.log('Coffee Deleted');
        })    
});