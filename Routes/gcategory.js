var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql querys for the categories
var insertCat = "INSERT INTO `gcategory`(name)Values(?)";
var getCatsById = "SELECT * FROM `gcategory` WHERE catID =?";
var getAllCats = "SELECT * FROM `gcategory`";
var deleteCategory = "DELETE FROM `gcategory` WHERE catID = ?";
var updateCategory = "UPDATE `gcategory`  SET name = ? WHERE catID=?";
//define bean routes

router.route('/cat/add')
    //add cat to server
    .post(function (req, res) {
        var catName = req.body.name;
        pool.query(
            insertCat, [catName],
            function (error, results, fields) {
                if (error) throw error;
                res.json({'name':req.body.name,'insertID':results.insertId});
            });
    });
router.route('/cat/')
    //get all cats
    .get(function (req, res) {
        pool.query(getAllCats, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });
    });

router.route('/cat/:id')
    //get category by id
    .get(function (req, res) {
        var usercatID = req.params.id;
        pool.query(getCatsById, usercatID, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });

    });
router.route('/cat/update/:id')
    //update category by id    
    .put(function (req, res) {
        var bcatID = req.params.id;
        var bname = req.body.name;
        pool.query(
            updateCategory, [bname, bcatID],
            function (error, results, fields) {
                if (error) throw error;
                res.json(req.body);
            });
    });

router.route('/cat/del/:id')
    //delete category
    .delete(function (req, res) {
        var usercatID = req.params.id;
        pool.query(deleteCategory, usercatID, function (error, results, fields) {
            if (error) throw error;
            res.json({id: req.params.id, isDeleted:'true'});
        });
    });