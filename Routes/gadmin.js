var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql querys for the admins
var insertAdmin = "INSERT INTO `gadmin`(name,school)Values(?,?)";
var getSchoolsById = "SELECT * FROM `gadmin` WHERE adminID =?";
var getAllAdmins = "SELECT * FROM `gadmin`";
var deleteAdmin = "DELETE FROM `gadmin` WHERE adminID = ?";
var updateAdmin = "UPDATE `gadmin`  SET name = ?, school= ? WHERE adminID =?";
//define admin routes

router.route('/admin/add')
    //add admin to server
    .post(function (req, res) {
        console.log(req.body);
        var adminName = req.body.name;
        var school = req.body.school;
        pool.query(
            insertAdmin, [adminName, school],
            function (error, results, fields) {
                if (error) throw error;
                res.json("Admin_Added");
                console.log('New Admin added');
            });
    });
router.route('/admin/')
    //get all admins
    .get(function (req, res) {
        pool.query(getAllAdmins, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
            console.log('Getting All Admins');
        });
    });

router.route('/admin/:id')
    //get admin by id
    .get(function (req, res) {
        var useradminID = req.params.id;
        pool.query(getSchoolsById, useradminID, function (error, results, fields) {
            if (error) throw error;
            console.log('Coffee Found');
            res.json(results);
        });

    });
router.route('/admin/update/:id')
    //update admin by id    
    .post(function (req, res) {
        console.log(req.body);
        var badminID = req.params.id;
        var bname = req.body.name;
        var bschool = req.body.school;
        pool.query(
            updateAdmin, [bname, bschool, badminID],
            function (error, results, fields) {
                if (error) throw error;
                res.json("admin Update");
                console.log('admin updated');
            });
    });

router.route('/admin/del/:id')
    //delete admin by id
    .post(function (req, res) {
        var useradminID = req.params.id;
        pool.query(deleteAdmin, useradminID, function (error, results, fields) {
            if (error) throw error;
            console.log('Admin Deleted');
        });
    });