var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql querys for the scores
var insertTeacher = "INSERT INTO `gteacher`(name,admin)Values(?,?)";
var getteacherById = "SELECT * FROM `gteacher` WHERE teachID =?";
var getAllStuds = "SELECT * FROM `gteacher`";
var deleteTeacher = "DELETE FROM `gteacher` WHERE teachID = ?";
var updateTeacher = "UPDATE `gteacher`  SET name = ?, admin= ? WHERE teachID=?";
//define score routes

router.route('/teacher/add')
    //add teachers to server
    .post(function (req, res) {
        var teachName = req.body.name;
        var teachAdmin = req.body.admin;
        pool.query(
            insertTeacher, [teachName, teachAdmin],
            function (error, results, fields) {
                if (error) throw error;
                rInsertID = 'insertID'+':'+results.insertId;
                res.json({'name':req.body.name, 'admin':req.body.admin,'insertID':results.insertId});
            });
    });
router.route('/teacher/')
    //get all teacher
    .get(function (req, res) {
        pool.query(getAllStuds, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });
    });

router.route('/teacher/:id')
    //get teachers by id
    .get(function (req, res) {
        var userteachID = req.params.id;
        pool.query(getteacherById, userteachID, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
        });

    });
router.route('/teacher/update/:id')
    //update teacher by id    
    .put(function (req, res) {
        var teachName = req.body.name;
        var teachAdmin = req.body.admin;
        var userteachID = req.params.id;
        pool.query(
            updateTeacher, [teachName, teachAdmin, userteachID],
            function (error, results, fields) {
                if (error) throw error;
                res.json(req.body);
            });
    });

router.route('/teacher/del/:id')
    //delete teacher
    .delete(function (req, res) {
        var userteachID = req.params.id;
        pool.query(deleteTeacher, userteachID, function (error, results, fields) {
            if (error) throw error;
            res.json({id: req.params.id, isDeleted:'true'});
        });
    });