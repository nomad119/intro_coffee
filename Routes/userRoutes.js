var express = require('express');
var app = module.exports= express();
var router= express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);
var mysql   = require('mysql');
var pool = mysql.createPool({
    host     :'localhost',
    user     :'root',
    password :'admin',
    database :'coffee'
});

//mysql querys for users
var insertUser = "INSERT INTO `couser`(Fname,Lname)Values(?,?)";
var getuserById = "SELECT * FROM `couser` WHERE userID =?";
var getuserByName = "SELECT * FROM `couser` WHERE Fname =?";
var getAllUsers = "SELECT * FROM `couser`";
var getUserRecipes = "Select * FROM `recipe` WHERE userID =?"
var deleteUser = "DELETE FROM `couser` WHERE userID = ?";
var deleteUserRecipes = "DELETE FROM `recipe` WHERE userID= ?";
var updateUser = "UPDATE `couser`  SET Fname = ?, Lname= ? WHERE userID = ?"
var checkUser =  "SELECT EXISTS(SELECT 1 FROM `couser` WHERE Fname =?) AS mycheck"

//define users routes

router.route('/cousers/add')
//add user to server
    .post(function(req,res){
        console.log(req.body);
        var Fname = pool.escape(req.body.Fname);
        var Lname = pool.escape(req.body.Lname);
        pool.query(
            insertUser,
            [Fname,Lname],
            function(error,results,fields){
            if(error) throw error;
            res.json("User_added");
            console.log('New User added');
        })
    });

router.route('/cousers/')
.get(function(req,res){
    pool.query(getAllUsers,function(error,results,fields){
        if(error) throw error;
        res.json(results);
        console.log('Getting All Users');
    }) 
});

router.route('/cousers/:id')
//get recipes by userid
    .get(function(req,res){
        var userID=req.params.id
        pool.query(getUserRecipes,userID,function(error,results,fields){
            if(error) throw error;
            console.log('Recipes Found');
            res.json(results);
        })
});
    router.route('/cousers/update/:id')
//update user by id    
    .post(function(req,res){
        console.log(req.body);
        var userID = req.body.id;
        var Fname = pool.escape(req.body.Fname);
        var Lname = pool.escape(req.body.Lname);
        pool.query(
            updateUser,
            [Fname,Lname,userID],
            function(error,results,fields){
            if(error) throw error;
            res.json("user_updated");
            console.log('updated user');
        })
});
router.route('/cousers/check/:username')
//get recipes by userid
    .post(function(req,res){
	console.log('stay awhile');
        var userID=req.params.username;
	console.log(userID);
        pool.query(checkUser,userID,function(error,results,fields){
            console.log(results[0].mycheck);
            if(error) throw error;
    if(results[0] !=0){
        pool.query(getuserByName,userID,function(error,results,fields){
            console.log(results);
            if(error) throw error;
            res.json(results);
        })
         
        }
        })
});
//delete recipe by id
router.route('/cousers/del/:id')
    .post(function(req,res){
        var userID=req.params.id;
        console.log(userID);
        pool.query(deleteUserRecipes,userID,function(error,results,fields){
            if(error) throw error;
            console.log('Recipe Deleted');
        })   
        pool.query(deleteUser,userID,function(error,results,fields){
            if(error) throw error;
            res.json("user deleted and all associated recipess");
            console.log('Recipe Deleted');
        })    
});