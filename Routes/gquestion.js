var express = require('express');
var app = module.exports = express();
var router = express.Router();
var bodyParser = require('body-parser');
//define the response and request data format.
//--------------------------------------------- 
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/api', router);
var mysql = require('mysql');
var pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'admin',
    database: 'game'
});

//mysql queries for the questions
var insertQuestion = "INSERT INTO `gquestion`(questBody,teachID,modID)Values(?,?,?)";
var getQuestionById = "SELECT * FROM `gquestion` WHERE questionID =?";
var getQuestionByTeacherId = "SELECT * FROM `gquestion` WHERE teachID =?";
var getQuestionByTeacherModId = "SELECT * FROM `gquestion` WHERE teachID =? AND modID=?";
var getAllQuestion = "SELECT * FROM `gquestion`";
var getQuestionsByTeacher = "SELECT * FROM `gquestion` WHERE teachID =? AND modID =?";
var deleteQuestion = "DELETE FROM `gquestion` WHERE questionID = ?";
var updateQuestion = "UPDATE `gquestion`  SET questBody = ?, teachID= ?, modID=? WHERE questionID=?";

//mysql queries for the answers
var insertAnswer = "INSERT INTO `gAnswer`(answerBody,modID,questionID)Values(?,?,?)";
var getAnswerById = "SELECT * FROM `gAnswer` WHERE answerID =?";
var getAllAnswers = "SELECT * FROM `gAnswer`";
var getAnswerssByQuestion = "SELECT * FROM `ganswer` WHERE questionID =? AND modID =?";
var deleteAnswer = "DELETE FROM `ganswer` WHERE questionID = ?";
var updateAnswer = "UPDATE `ganswer`  SET answerBody = ?, modID= ?, questionID=? WHERE answerID=?";

//define question routes
router.route('/question/add')
    //add score to server
    .post(function (req, res) {
        console.log(req.body);
        var questBody = req.questBody;
        var teachID = req.teachID;
        var modID = req.modID;
        pool.query(
            insertQuestion, [questBody, teachID, modID],
            function (error, results, fields) {
                if (error) throw error;
                res.json("Question_Added" + ":" + questBody + teachID + modID);
                console.log('Question Add' + results.insertID);
            });
    });
router.route('/question/')
    //get all scores
    .get(function (req, res) {
        pool.query(getAllScores, function (error, results, fields) {
            if (error) throw error;
            res.json(results);
            console.log('Getting All Scores');
        });
    });

router.route('/question/teacher/id')
    //get questions by teacher
    .get(function (req, res) {
        var teacherID = req.param.id;
        pool.query(getQuestionByTeacherId, [teacherID], function (error, results, fields) {
            if (error) throw error;
            res.json(results);
            console.log('Getting All the questions by teacher:' + teacherID);
        });
    });
router.route('/question/teacher/id')
    //get questions by teacher and by module
    .get(function (req, res) {
        var teacherID = req.param.id;
        var modID = req.modID;
        pool.query(getQuestionByTeacherId, [teacherID, modID], function (error, results, fields) {
            if (error) throw error;
            res.json(results);
            console.log('Getting All the questions from module: ' + modID + ' by teacher:' + teacherID);
        });
    });

router.route('/question/:id')
    //get score by id
    .get(function (req, res) {
        var userscoreID = req.params.id;
        pool.query(getScoresById, userscoreID, function (error, results, fields) {
            if (error) throw error;
            console.log('Scores Found');
            res.json(results);
        });
    });
router.route('/question/update/:id')
    //update Score by id    
    .post(function (req, res) {
        console.log(req.body);
        var questionID = req.param.id;
        var questBody = req.questBody;
        var teachID = req.teachID;
        var modID = req.modID;
        pool.query(
            insertQuestion, [questBody, teachID, modID, modID],
            function (error, results, fields) {
                if (error) throw error;
                res.json("Question_modified" + ":" + questBody + teachID + modID);
                console.log('Question modified' + results.insertID);
            });
    });

router.route('/question/del/:id')
    //delete Score
    .post(function (req, res) {
        var questionID = req.params.id;
        pool.query(deleteQuestion, questionID, function (error, results, fields) {
            if (error) throw error;
            console.log('Question Deleted');
        });
    });

router.route('/qna/create')
    //create new question and answers associated with it
    .post(function (req, res) {
        var teacherID = req.teacherID;
        var modID = req.modID;
        var questBody = req.questions;
        //once the question is successfully inserted the answers are inserted
        //with the last id inserted based on connnection
        pool.query(insertQuestion, questBody.questionBody, teacherID, modID, function (err, result) {
            console.log('questions added');
            if (err) {
                console.log(err);
            }
            //result.insertID is returning from the database with the last inserted id.
            else {
                json.forEach(function (questBody.answers) {
                    pool.query(insertAnswer, answer.answerBody, modID, result.insertID, function (error, results, fields) {
                        if (error) throw error;
                        console.log('answers added');
                    });
                });
            }
        });

    });


router.route('/questionans/update/:id')
    //create new question and answers associated with it
    .post(function (req, res) {
        var questionID = req.params.id;
        var teacherID = req.teacherID;
        var modID = req.modID;
        var questBody = req.questions;
        //once the question is successfully inserted the answers are inserted
        //with the last id inserted based on connnection
        pool.query(updateQuestion, questBody.questionBody, teacherID, modID, function (err, result) {
            console.log('questions added');
            if (err) {
                console.log(err);
            }
            //result.insertID is returning from the database with the last inserted id.
            else {
                json.forEach(function (questBody.answers) {
                    pool.query(insertAnswer, answer.answerBody, modID, result.insertID, function (error, results, fields) {
                        if (error) throw error;
                        console.log('answers added');
                    });
                });
            }
        });
    });