var Student = require('mongoose').model('Student');
//Post call for the studen
exports.createStudent = function(req,res){
  var student = new Student();
  student.fName = req.body.fName;
  student.lName = req.body.lName;
  student.status = req.body.status;
  console.log(req.body);
  //save the student and check for errors
  student.save(function(err){
      if(err){
        res.send(err);
      }

      res.json({message: 'Student created!'});
  });

},
exports.getAllStudents= function(req,res){
  Student.find(function(err, students){
    if(err){
      res.send(err);
    }
    res.json(students);
  })
},
exports.getStudent= function(req,res){
  Student.findById(req.params.student_id, function(err, student){
      if(err){
        res.send(err);
      }
      res.json(student);

  });
},
exports.updateStudent= function(req,res){
  Student.findById(req.params.student_id, function(err, student){
    if(err){
      res.send(err);
    }
    res.json({message: 'Student updated!'});
  });
},
exports.deleteStudent = function(req,res){
  Student.remove({
    _id: req.params.student_id
  }, function(err, student){
    if(err){
      res.send(err);
    }
    res.json({message : 'Successfully deleted'});
  });
};
